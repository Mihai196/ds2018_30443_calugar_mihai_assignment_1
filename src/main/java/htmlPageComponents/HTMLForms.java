package htmlPageComponents;

public class HTMLForms {
    public static String getClientForm(){
        return "<div class=\"form\">" +
                "<form action=\"/client\" method=\"POST\">\n" +
                "FlightIdTimeDetails: <input type=\"text\" name=\"flightId\">\n" +
                "<br>\n" +
                "<input type=\"submit\" name=\"action\" value=\"QueryWebService\">\n" +
                "</form>";
    }
    public static String getAdminForm(){
        return "<div class=\"form\">" +
                "<form action=\"/flight\" method=\"POST\">\n" +
                "Id: <input type=\"text\" name=\"flightId\">\n" +
                "<br>\n" +
                "Airplane Type: <input type=\"text\" name=\"airplaneType\">\n" +
                "<br>\n" +
                "DepartureCityId: <input type=\"text\" name=\"departureCity\">\n" +
                "<br>\n" +
                "Departure time: <input type=\"datetime-local\" name=\"departureDate\"/>" +
                "<br>\n" +
                "Arrival cityId: <input type=\"text\" name=\"arrivalCity\">\n " +
                "<br>\n" +
                "Arrival time: <input type=\"datetime-local\" name=\"arrivalDate\"/>" +
                "<br>\n" +
                "<input type=\"submit\" name=\"action\" value=\"Insert\">\n" +
                "<input type=\"submit\" name=\"action\" value=\"Update\">\n" +
                "<input type=\"submit\" name=\"action\" value=\"Delete\">\n" +
                "</form>";
    }
    public static String getLoginForm(){
        return                 "      <form action = \"/user\" method = \"POST\">\n" +
                "         Username: <input type = \"text\" name = \"username\">\n" +
                "         <br />\n" +
                "         Password: <input type = \"password\" name = \"password\" />\n" +
                "         <br>" +
                "         <input type = \"submit\" value = \"Login\" />\n" +
                "      </form>\n";
    }
}
