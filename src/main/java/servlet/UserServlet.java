package servlet;

import dao.UserDAO;
import entity.User;
import htmlPageComponents.HTMLForms;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class UserServlet extends HttpServlet {

    private UserDAO userDAO;
    public void init(){
        userDAO=new UserDAO(new Configuration().configure().buildSessionFactory());
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorMessage;
        if(request.getSession().getAttribute("ErrorMessage")==null){
            errorMessage="";
        }
        else{
            errorMessage=request.getSession().getAttribute("ErrorMessage").toString();
        }

        String page = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n" +
                "<html>\n" +
                "   <body>\n" +
                errorMessage+
                HTMLForms.getLoginForm() +
                "   </body>\n" +
                "</html>";
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println(page);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        User user=userDAO.findByUsername(username);
        if(user!=null&&user.getPassword().equals(password)) {
            System.out.println(user.toString());
            if (user.getRole().equals("administrator")) {
                request.getSession().setAttribute("loggedUser", username);
                request.getSession().setAttribute("loggedUserRole", "administrator");
                response.sendRedirect("flight");
            } else {
                request.getSession().setAttribute("loggedUser", username);
                request.getSession().setAttribute("loggedUserRole", "client");
                response.sendRedirect("client");
            }
        }
        else{
            request.getSession().setAttribute("ErrorMessage","Invalid username or password");
            response.sendRedirect("user");
        }
    }

}
