package servlet;

import dao.CityDAO;
import dao.FlightDAO;
import entity.City;
import entity.Flight;
import htmlPageComponents.HTMLForms;
import htmlPageComponents.HTMLTables;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

public class FlightServlet extends HttpServlet{

    private FlightDAO flightDAO;
    private CityDAO cityDAO;
    public void init (){
        flightDAO=new FlightDAO(new Configuration().configure().buildSessionFactory());
        cityDAO=new CityDAO(new Configuration().configure().buildSessionFactory());
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String action=request.getParameter("action");
        try {
            switch (action) {
                case "Insert":
                    insertFlight(request, response);
                    break;
                case "Delete":
                    deleteFlight(request,response);
                    break;
                case "Update":
                    updateFlight(request,response);
                    break;
                    default:
                        break;
            }
        }
        catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.getSession().getAttribute("loggedUser")!=null&&
                request.getSession().getAttribute("loggedUserRole").equals("administrator")) {

            String errorMessage;
            if(request.getSession().getAttribute("errorInsertMessage")==null){
                errorMessage="";
            }
            else{
                errorMessage=request.getSession().getAttribute("errorInsertMessage").toString();
            }
            String adminPage =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n" +
                            "<html>" +
                            "<body>" +
                            "<div class=\"table\">" +
                            "       Flights:" +
                            "<br>" +
                            errorMessage+
                            HTMLTables.getTableFlights() +
                            "<div>" +
                            "Cities" +
                            HTMLTables.getTableCities() +
                            "</div>" +
                            "</div>" +
                            "<br>" +
                            "<br>" +
                            HTMLForms.getAdminForm() +
                            "</div>" +
                            "</body>" +
                            "</html>";
            response.setContentType("text/html");

            PrintWriter out = response.getWriter();
            out.println(adminPage);
        }
        else{
            response.sendRedirect("user");
        }
    }
    private void insertFlight(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String name = request.getParameter("airplaneType");

        String departureDate = request.getParameter("departureDate");
        if (StringUtils.countMatches(departureDate, ":") == 1) {
            departureDate += ":00";
        }
        Date departureDateDB=Timestamp.valueOf(departureDate.replace("T"," "));

        int departureCityId = Integer.parseInt(request.getParameter("departureCity"));

        String arrivalDate = request.getParameter("arrivalDate");
        if (StringUtils.countMatches(arrivalDate, ":") == 1) {
            arrivalDate += ":00";
        }
        Date arrivalDateDB=Timestamp.valueOf(arrivalDate.replace("T"," "));

        int arrivalCityId = Integer.parseInt(request.getParameter("arrivalCity"));
        City departureCity=cityDAO.findById(departureCityId);
        City arrivalCity=cityDAO.findById(arrivalCityId);
        if(departureCity!=null&&arrivalCity!=null) {
            Flight flight = new Flight(name, departureCity, departureDateDB, arrivalCity, arrivalDateDB);
            boolean flightok = flightDAO.insertFlight(flight);
            System.out.println(flightok);
            response.sendRedirect("flight");
        }
        else
        {
            request.getSession().setAttribute("errorInsertMessage","Make sure you give valid fields(eg. valid id fields for cities)");
            response.sendRedirect("flight");
        }
    }

    private void deleteFlight(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {

        int id = Integer.parseInt(request.getParameter("flightId"));
        Flight flight=new Flight(id);
        flightDAO.deleteFlight(flight);
        response.sendRedirect("flight");
    }

    private void updateFlight(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        int flightId=Integer.parseInt(request.getParameter("flightId"));
        String name = request.getParameter("airplaneType");
        String departureDate = request.getParameter("departureDate");
        int departureCityId = Integer.parseInt(request.getParameter("departureCity"));
        String arrivalDate = request.getParameter("arrivalDate");
        int arrivalCityId = Integer.parseInt(request.getParameter("arrivalCity"));
        City departureCity=cityDAO.findById(departureCityId);
        City arrivalCity=cityDAO.findById(arrivalCityId);
        Flight flight=new Flight(flightId,name,departureCity,new Date(),arrivalCity,new Date());
        flightDAO.updateFlight(flight);
        response.sendRedirect("flight");
    }
}
